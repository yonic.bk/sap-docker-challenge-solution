from flask.cli import FlaskGroup
from src import create_app, db

# Creates the task table
from src.api.models import Task

cli = FlaskGroup(create_app=create_app)
app = create_app()


@cli.command('recreate_db')
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command('seed_db')
def seed_db():
    db.session.add(Task(name='Win ipad', description="Me trying to win ipad"))
    db.session.add(Task(name='Win airpods', description="Me trying to win airpods"))
    db.session.commit()


if __name__ == '__main__':
    cli()
