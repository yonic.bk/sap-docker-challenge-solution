from flask import Blueprint, request
from flask_restx import Api, Resource, fields

from src.api.crud import (
    get_all_tasks,
    add_task,
    get_task_by_id,
    update_task,
    delete_task,
)

tasks_blueprint = Blueprint("tasks", __name__)
api = Api(tasks_blueprint)

task = api.model(
    "Task",
    {
        "id": fields.Integer(readonly=True),
        "name": fields.String(required=True),
        "description": fields.String(required=True),
        "created_date": fields.DateTime,
    },
)


@api.route("/tasks/<int:task_id>")
class Tasks(Resource):
    @api.marshal_with(task)
    def get(self, task_id: int):
        task = get_task_by_id(task_id)
        if not task:
            api.abort(404, f"User {task_id} does not exist")
        return task, 200

    def delete(self, task_id: int):
        task = get_task_by_id(task_id)
        if not task:
            api.abort(404, f"User {task_id} does not exist")

        delete_task(task)
        return {"message": f"{task.name} was removed!"}, 200

    @api.expect(task, validate=True)
    def put(self, task_id: int):
        post_data = request.get_json()
        name = post_data.get("name")
        description = post_data.get("description")

        task = get_task_by_id(task_id)
        if not task:
            api.abort(404, f"Task {task_id} does not exist")

        update_task(task, name, description)

        return {"message": f"{task_id} was updated!"}, 200


@api.route("/tasks")
class TaskList(Resource):
    @api.marshal_with(task, as_list=True)
    def get(self):
        return get_all_tasks(), 200

    @api.expect(task, validate=True)
    def post(self):
        post_data = request.get_json()
        name = post_data.get("name")
        description = post_data.get("description")

        add_task(name, description)

        return {"message": f"{name} was added!"}, 201
