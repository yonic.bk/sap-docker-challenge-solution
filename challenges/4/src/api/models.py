from sqlalchemy import func

from src import db


class Task(db.Model):
    __tablename__ = "tasks"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(128), nullable=False)
    description = db.Column(db.String(128), nullable=False)
    creation_date = db.Column(db.DateTime, default=func.now(), nullable=False)

    def __init__(self, name, description):
        self.name = name
        self.description = description

