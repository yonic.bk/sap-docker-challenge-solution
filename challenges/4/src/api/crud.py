from src import db
from src.api.models import Task


def get_all_tasks():
    return Task.query.all()


def get_task_by_id(task_id: int):
    return Task.query.filter_by(id=task_id).first()


def add_task(name, description):
    task = Task(name=name, description=description)
    db.session.add(task)
    db.session.commit()
    return task


def update_task(task, name, description):
    task.name = name
    task.description = description
    db.session.commit()
    return task


def delete_task(task):
    db.session.delete(task)
    db.session.commit()
    return task
